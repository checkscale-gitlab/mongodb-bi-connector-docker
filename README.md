# Mongodb BI Connector

Imagen Docker de  mongodb-bi-connector.

## Security / Auth

Este Mongodb BI Connector se asume que su servidor mongodb puede conectarse sin ninguna autenticación (red confiable).
"*La autenticación en MongoDB es bastante compleja*". Esa es la razón por la que hacer que este Mongodb BI Connector sea lo más simple posible por ahora.

## docker-compose example

```
version: "3"
services:
  mongodb:
    image: mongo:bionic
  
  mongodb-bi-connector:
    image: registry.gitlab.com/huezo/mongodb-bi-connector-docker:latest
    environment:
      MONGODB_HOST: mongodb
      MONGODB_PORT: 27017  

```

#docker-compose

```
version: "3"
services:
  mongodb:
    image: mongo:bionic
    restart: always
    ports:
      - "27017:27017"
    volumes:
      - ./mongo/mongo-volumen:/data/db
      - ./mongo/mongo-config-volumen:/data/configdb
  mongodb-bi-connector:
    image: registry.gitlab.com/huezo/mongodb-bi-connector-docker:latest
    restart: always
    ports:
      - "3307:3307"
    environment:
      MONGODB_HOST: mongodb
      MONGODB_PORT: 27017  
      
```

#docker-compose mongodb-bi-connector + MongoDB + CubeJS


```
version: "3"
services:
  mongodb:
    image: mongo:bionic
    restart: always
    ports:
      - "27017:27017"
    volumes:
      - ./mongo/mongo-volumen:/data/db
      - ./mongo/mongo-config-volumen:/data/configdb
  mongodb-bi-connector:
    image: registry.gitlab.com/huezo/mongodb-bi-connector-docker:latest
    restart: always
    ports:
      - "3307:3307"
    environment:
      MONGODB_HOST: mongodb
      MONGODB_PORT: 27017  
  cube:
    image: cubejs/cube:v0.28.59
    restart: always
    ports:
      # It's better to use random port binding for 4000/3000 ports
      # without it you will not able to start multiple projects inside docker
      - 4000:4000  # Cube.js API and Developer Playground
      - 3000:3000  # Dashboard app, if created
    env_file: .env
    volumes:
      - .:/cube/conf
      # We ignore Cube.js deps, because they are built-in inside the official Docker image
      - .empty:/cube/conf/node_modules/@cubejs-backend/

```

# Desplegar 

```docker-compose up -d```